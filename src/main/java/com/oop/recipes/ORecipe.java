package com.oop.recipes;

import org.bukkit.inventory.ItemStack;

public class ORecipe {
    private ItemStack[][] matrix;

    private ItemStack result;

    protected ORecipe(ItemStack[][] matrix, ItemStack result) {
        this.matrix = matrix;
        this.result = result;
    }

    public int getLen() {
        return matrix.length;
    }

    public boolean is3x3() {
        return matrix.length == 3;
    }

    public static RecipeBuilder builder() {
        return new RecipeBuilder();
    }

    public ItemStack[][] getMatrix() {
        return this.matrix;
    }

    public ItemStack getResult() {
        return this.result;
    }

    public void setResult(ItemStack result) {
        this.result = result;
    }
}
