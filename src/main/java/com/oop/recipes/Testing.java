package com.oop.recipes;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Testing {
    public static void main(String[] args) {
        ORecipe recipe = ORecipe
                .builder()
                .pattern("D X A", "A X A")
                .item('D', new ItemStack(Material.DIAMOND))
                .item('A', new ItemStack(Material.ARROW))
                .result(new ItemStack(Material.ANVIL))
                .build();

        RecipesController.getInstance().register(recipe);
    }
}
