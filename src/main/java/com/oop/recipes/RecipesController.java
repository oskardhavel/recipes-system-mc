package com.oop.recipes;

import com.google.common.collect.Sets;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class RecipesController implements Listener {
    private static RecipesController instance;

    private final List<MatrixPacked> slots2x2in3x3 = new ArrayList<>();
    private final Set<ORecipe> recipes = Sets.newConcurrentHashSet();

    static {
        new RecipesController();
    }

    private RecipesController() {
        instance = this;
    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onCraft(PrepareItemCraftEvent event) {
        final ItemStack[] inv_matrix = event.getInventory().getMatrix();
        int len = event.getInventory().getSize() == 5 ? 2 : 3;

        for (ORecipe recipe : recipes) {
            if (recipe.is3x3() && len == 2) return;

            if (recipe.getLen() == len) {
                if (compareMatrixes(recipe, inv_matrix)) {
                    event.getInventory().setResult(recipe.getResult());
                    break;
                }

            } else {
                // We possibly got 2x2 in 3x3
                ItemStack[][] matrix = recipe.getMatrix();
                ItemStack recipe_current = matrix[0][0];
                ItemStack recipe_left = matrix[0][1];
                ItemStack recipe_down = matrix[1][0];
                ItemStack recipe_diag = matrix[1][1];

                boolean recipeFound = false;
                for (MatrixPacked matrixPacked : slots2x2in3x3) {
                    ItemStack inv_current = inv_matrix[matrixPacked.getCurrent()];
                    ItemStack inv_left = inv_matrix[matrixPacked.getLeft()];
                    ItemStack inv_down = inv_matrix[matrixPacked.getDown()];
                    ItemStack inv_diag = inv_matrix[matrixPacked.getDiag()];

                    if (itemArrayEquals(recipe_current, recipe_left, recipe_down, recipe_diag, inv_current, inv_left, inv_down, inv_diag)) {
                        event.getInventory().setResult(recipe.getResult());
                        recipeFound = true;
                        break;
                    }
                }

                if (recipeFound)
                    break;
            }
        }
    }

    public Optional<ORecipe> getRecipeFromResult(ItemStack itemStack) {
        return recipes
                .stream()
                .filter(recipe -> recipe.getResult().equals(itemStack))
                .findFirst();
    }

    public void register(ORecipe recipe) {
        register(new ORecipe[]{recipe});
    }

    public void register(ORecipe ...recipes) {
        this.recipes.addAll(new ArrayList<>(Arrays.asList(recipes)));
    }

    public static RecipesController getInstance() {
        return instance;
    }

    // Utilities

    private boolean compareRecipeItem(ItemStack recipe_itemStack, ItemStack inventory_itemStack) {
        if (recipe_itemStack == null || recipe_itemStack.getType() == Material.AIR)
            return inventory_itemStack == null || inventory_itemStack.getType() == Material.AIR;

        if (inventory_itemStack == null || inventory_itemStack.getType() == Material.AIR) return false;
        if (recipe_itemStack.getAmount() > inventory_itemStack.getAmount()) return false;
        if (recipe_itemStack.getType() != inventory_itemStack.getType()) return false;

        return recipe_itemStack.isSimilar(inventory_itemStack);
    }

    private boolean itemArrayEquals(ItemStack... itemStacks) {
        int mid = itemStacks.length / 2;

        ItemStack[] right = Arrays.copyOfRange(itemStacks, 0, mid);
        ItemStack[] left = Arrays.copyOfRange(itemStacks, mid, itemStacks.length);

        for (int i = 0; i < mid; i++) {
            if (!compareRecipeItem(right[i], left[i]))
                return false;
        }

        return true;
    }

    private boolean compareMatrixes(ORecipe recipe, ItemStack[] inv_matrix) {
        ItemStack[][] matrix = recipe.getMatrix();
        int len = matrix.length;
        boolean break_bool = false;

        for (int y = 0; y < len; y++) {

            for (int x = 0; x < len; x++) {
                ItemStack recipe_itemStack = matrix[y][x];
                int slot = y == 0 ? x : y * x;
                ItemStack inventory_itemStack = inv_matrix[slot];

                if (!compareRecipeItem(recipe_itemStack, inventory_itemStack)) {
                    break_bool = true;
                    break;
                }
            }
            if (break_bool)
                break;
        }
        return !break_bool;
    }
}
