package com.oop.recipes;

public class MatrixPacked {
    int current;
    int left;
    int down;
    int diag;

    public MatrixPacked(int current, int left, int down, int diag) {
        this.current = current;
        this.left = left;
        this.down = down;
        this.diag = diag;
    }

    @Override
    public String toString() {
        return "MatrixPacked{" +
                "current=" + current +
                ", left=" + left +
                ", down=" + down +
                ", diag=" + diag +
                '}';
    }

    public int getCurrent() {
        return this.current;
    }

    public int getLeft() {
        return this.left;
    }

    public int getDown() {
        return this.down;
    }

    public int getDiag() {
        return this.diag;
    }
}
